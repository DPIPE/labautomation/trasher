from typing import TYPE_CHECKING

from watchfiles import Change

from backend.observer import observe

if TYPE_CHECKING:
    from .conftest import MockRustType


async def test_observe_captures_the_right_file_types(
    mock_rust_notify: "MockRustType", mocker
):
    mock_rust_notify(
        [
            {
                (Change.added, "foo.bam"),
                (Change.modified, "foo.bai"),
                (Change.deleted, "foo.vcf"),
                (Change.added, "foo.gvcf"),
                (Change.added, "foo.txt"),
                (Change.modified, "foo.py"),
            }
        ]
    )
    stub = mocker.stub()
    await observe(".", handler=stub)
    stub.assert_called_once_with(
        {
            (Change.added, "foo.bam"),
            (Change.modified, "foo.bai"),
            (Change.deleted, "foo.vcf"),
            (Change.added, "foo.gvcf"),
        }
    )
