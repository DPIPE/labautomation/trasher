from pathlib import Path

from watchfiles import Change

import backend
from backend.walker import walk


def test_walker_captures_the_right_file_types(mocker):
    test_files = Path(backend.__file__).parent.parent / "tests" / "test_files"
    stub = mocker.stub()
    walk(test_files, handler=stub)
    stub.assert_called_once_with(
        set(
            [
                (
                    Change.added,
                    Path(test_files / "a.vcf"),
                ),
                (
                    Change.added,
                    Path(test_files / "c.bam"),
                ),
                (
                    Change.added,
                    Path(test_files / "dir_a" / "a.bai"),
                ),
                (
                    Change.added,
                    Path(test_files / "dir_a" / "c.gvcf"),
                ),
                (
                    Change.added,
                    Path(test_files / "dir_b" / "a.vcf"),
                ),
                (
                    Change.added,
                    Path(test_files / "dir_b" / "c.bam"),
                ),
            ]
        )
    )
