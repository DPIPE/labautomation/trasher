from typing import TYPE_CHECKING, Any, List, Set, Tuple

import pytest


@pytest.fixture(autouse=True)
def anyio_backend():
    return "asyncio"


ChangesType = List[Set[Tuple[int, str]]]


class MockRustNotify:
    def __init__(self, changes: ChangesType, exit_code: str):
        self.iter_changes = iter(changes)
        self.exit_code = exit_code
        self.watch_count = 0

    def watch(self, debounce_ms: int, step_ms: int, timeout_ms: int, cancel_event):
        try:
            change = next(self.iter_changes)
        except StopIteration:
            return self.exit_code
        else:
            self.watch_count += 1
            return change

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def close(self):
        pass


if TYPE_CHECKING:
    from typing import Literal, Protocol

    class MockRustType(Protocol):
        def __call__(
            self,
            changes: ChangesType,
            *,
            exit_code: Literal["signal", "stop", "timeout"] = "stop",
        ) -> Any:
            ...


@pytest.fixture
def mock_rust_notify(mocker):
    def mock(changes: ChangesType, *, exit_code: str = "stop"):
        m = MockRustNotify(changes, exit_code)
        mocker.patch("watchfiles.main.RustNotify", return_value=m)
        return m

    return mock
