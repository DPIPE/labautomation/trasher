from pathlib import Path

import pytest
from watchfiles import Change

from backend.filter import SampleFilter


@pytest.mark.parametrize(
    "path,expected",
    [
        ("foo.bam", True),
        ("foo.vcf", True),
        ("foo.txt", False),
        ("foo.vcf.more", False),
        (Path("x/y/z/foo.bai"), True),
        (Path.home() / "ignore" / "foo.bam", False),
        (Path.home() / "ignore", False),
        (Path.home() / ".git" / "foo.bam", False),
        (Path.home() / "foo" / "foo.txt", False),
        (Path(".git") / "foo.vcf", False),
    ],
)
def test_default_filter(path, expected):
    f = SampleFilter(ignore_paths=[Path.home() / "ignore"])
    assert f(Change.added, str(path)) == expected
