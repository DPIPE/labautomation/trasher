import os
from pathlib import Path
from typing import Callable, Optional, Union

from watchfiles import Change

from backend.filter import SampleFilter
from backend.handlers import null_handler


def walk(
    *paths: Union[Path, str],
    handler: Callable[[[Change, Path]], None] = null_handler,
    watch_filter: Optional[Callable[[Change, str], bool]] = SampleFilter(),
):

    """
    Walks through a list of directories and invokes the handler for each file
    that is found passing the filter.
    """
    found = set()
    for path in paths:
        for root, dirs, files in os.walk(path):
            found.update(
                [
                    (
                        Change.added,
                        Path(os.path.join(root, file)),
                    )
                    for file in files
                    if watch_filter(Change.added, file)
                ]
            )

    handler(found)
