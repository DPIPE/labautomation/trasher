import asyncio
from pathlib import Path
from typing import Callable, Optional, Union

from watchfiles import Change, awatch

from backend.filter import SampleFilter
from backend.handlers import null_handler


async def observe(
    *paths: Union[Path, str],
    handler: Callable[[Change, Path], None] = null_handler,
    watch_filter: Optional[Callable[[Change, str], bool]] = SampleFilter(),
):
    async for changes in awatch(*paths, watch_filter=watch_filter):
        handler(changes)


async def peek(
    *paths: Union[Path, str],
    handler: Callable[[Change, Path], None],
    duration: int = 3,
    watch_filter: Optional[Callable[[Change, str], bool]] = SampleFilter(),
):
    stop_event = asyncio.Event()

    async def stop_soon():
        await asyncio.sleep(duration)
        print("Stopping file watcher...")
        stop_event.set()

    stop_soon_task = asyncio.create_task(stop_soon())

    await observe(
        *paths,
        handler=handler,
        watch_filter=watch_filter,
    )

    # cleanup by awaiting the (now complete) stop_soon_task
    await stop_soon_task
