from pathlib import Path
from typing import Optional, Sequence, Union

from watchfiles import Change, DefaultFilter


class SampleFilter(DefaultFilter):
    """
    Default filter
    """

    def __init__(
        self,
        *,
        ignore_paths: Optional[Sequence[Union[str, Path]]] = None,
    ) -> None:
        """
        Args:
            ignore_paths: The paths to ignore, see [`BaseFilter`][watchfiles.BaseFilter].
        """
        self.extensions = (".bam", ".bai", ".vcf", ".gvcf")
        super().__init__(ignore_paths=ignore_paths)

    def __call__(self, change: "Change", path: str) -> bool:
        return path.endswith(self.extensions) and super().__call__(change, path)
