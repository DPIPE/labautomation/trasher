from typing import Sequence, Tuple

from watchfiles import Change


def null_handler(changes: Sequence[Tuple[Change, str]]):
    print("Changes found:", changes)
