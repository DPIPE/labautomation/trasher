import asyncio
from pathlib import Path
from typing import List

import typer

from backend.config import Config
from backend.handlers import null_handler
from backend.observer import observe
from backend.walker import walk as do_walk

app = typer.Typer()


@app.command()
def watch(paths: List[Path]):
    typer.echo(f"Observing paths: {paths}")
    asyncio.run(observe(*paths, handler=null_handler))


@app.command()
def walk(paths: List[Path] = Config.ROOT_FOLDERS):
    """
    Walk through the paths and list changes.
    """
    typer.echo(f"Walking through paths: {paths}")
    do_walk(*paths, handler=null_handler)


if __name__ == "__main__":
    app()
